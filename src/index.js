import React from 'react';
import ReactDOM from 'react-dom/client';

import {
  BrowserRouter,
  Routes,
  Route,
  Link
} from "react-router-dom";

import './index.css';
import Oak from './oak/Oak';
import reportWebVitals from './reportWebVitals';
/*
import bulba from './images/bulbassaur.png';

const bulbassaur = {
  img: bulba,
  species: 'Bulbassaur',
  //nick: 'Bubu'
}

const missigno = {
  img: null,
  species: 'Missigno',
  nick: '404'
}*/

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <div id='navbar'>
        <nav>
          <ul>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/Oak">Cadastrar espécies</Link></li>
          </ul>
        </nav>
      </div>
      <Routes>
        <Route path='/' element={<p>Oie</p>}/>
        <Route path='/Oak' element={<Oak/>}/>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
