
import './Pokemon.css'
import pokebola from '../images/pokeball.jpg'

const Pokemon = (props) => {
    const pokemon = props.pokemon;
    const species = pokemon.species || 'unknown specie';
    const photoUrl = pokemon.img || pokebola;
    const nick = pokemon.nick || '';
    return (
        <div>
            <h4>{species}</h4>
            {nick? <h4>{nick}</h4>: null}
            <img src={photoUrl} className='pokemon'></img>
        </div>
    );
}

export default Pokemon;