
import './PokeSpecies.css'
import pokebola from '../images/pokeball.jpg'

const PokeSpecies = (props) => {
    const pokemon = props.pokemon;
    const species = pokemon.species || 'unknown specie';
    const nationalPokedexId = pokemon.nationalPokedexId || null;
    const photoUrl = pokemon.img || pokebola;
    return (
        <div>
            <h4>{species}{nationalPokedexId? `#${nationalPokedexId}`: ''}</h4>
            <img src={photoUrl} className='pokemon'></img>
        </div>
    );
}

export default PokeSpecies;